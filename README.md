# retro-board

![Retrospected.com](/content/screenshot-v4.png?raw=true "Retrospected.com")

This project is both an actual product, and also a technology demo using the latest and greatest JavaScript libraries of the month.

It features the following technologies:

* [React](https://github.com/facebook/react)
* [Redux](https://github.com/reactjs/redux)
* [React Router 4](https://github.com/ReactTraining/react-router)
* [Socket IO](http://socket.io)
* [Webpack 3](https://github.com/webpack/webpack) (See older versions for Webpack 1)
* [Hot-reloading](https://webpack.github.io/docs/hot-module-replacement.html)
* [Material UI design](https://www.google.com/design/spec/material-design/introduction.html)
* [CSS Modules](https://github.com/css-modules/css-modules)
* [redux-saga](https://github.com/yelouafi/redux-saga)
* [reselect](https://github.com/reactjs/reselect)
* [Multilingual](https://stackoverflow.com/questions/33413880/react-redux-and-multilingual-internationalization-apps-architecture) / Internationalization
* [MongoDB](https://www.mongodb.org/)  
* [ESLint](http://eslint.org/) for JS and JSX
* [Jest](https://facebook.github.io/jest) for Unit Testing
* [Yarn](https://yarnpkg.com/en/) to replace NPM


## How to run for production

* Clone this repository
* `yarn` to install the dependencies (Node 8+, NPM 5+)
* `npm run build` to build everything (client and server)
* `npm start` to run the server on port 8080
* Open your browser on [http://localhost:8080](http://localhost:8080)


## How to run for development

* Clone this repository
* `yarn` to install the dependencies (or `npm i`) (Node 8+, NPM 5+)
* `npm run start-server` on the first terminal to start the server bit
* `npm run start-ui` on the second terminal, to run live webpack with hot-reload
* Open your browser on [http://localhost:8081](http://localhost:8081)


## How to run the tests

* Clone this repository
* `yarn` to install the dependencies (or `npm i`) (Node 8+, NPM 5+)
* `npm test` to run the tests

## How to debug

### Debugging the server

* Run `npm run start-server-debug`
* Don't forget to start the client side as well `npm run start-ui`

### Debugging the client

* Run both client and server normally (`npm run start-server` and `npm run start-ui`)
